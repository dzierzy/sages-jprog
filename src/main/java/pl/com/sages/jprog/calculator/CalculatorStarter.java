package pl.com.sages.jprog.calculator;


import pl.com.sages.jprog.calculator.better.BetterCalculator;

public class CalculatorStarter {

    public static void main(String[] args) {
        System.out.println("CalculatorStarter.main");

        Calculator c1 = new BetterCalculator(5);
        c1.loadFromDB();

        Calculator c2 = new BetterCalculator(-99);


        c1.add(1, 2,3,4,5,6);

        c1.add(1, new double[]{1,2,3,4,5});

        BetterCalculator bc = (BetterCalculator)c1;


        c1.multiply(3);
        c1.subtract(4.5);

        try {
            c1.divide(1.1);
        } catch (CalculatorException | IllegalArgumentException e) {
            e.printStackTrace();
        } /*catch (IllegalArgumentException e) {
            e.printStackTrace();
        } */finally {
            System.out.println("executed always");
        }

        if(c1 instanceof BetterCalculator) {
            System.out.println("power");
            ((BetterCalculator) c1).power(2);
        }
        c1.displayResult();
        System.out.println("c1=" + c1);

        c2.add(2);
        c2.add(2);
        c2.displayResult();
        System.out.println("c2: " + c2 );

        int r = c1.multiply(2,3);


        c1.save();

    }
}
