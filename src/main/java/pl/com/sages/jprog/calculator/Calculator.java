package pl.com.sages.jprog.calculator;


import pl.com.sages.jprog.calculator.better.BetterCalculator;
import pl.com.sages.jprog.calculator.memory.ArrayMemory;
import pl.com.sages.jprog.calculator.memory.ListMemory;
import pl.com.sages.jprog.calculator.memory.Memory;

import javax.swing.plaf.nimbus.State;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

public abstract class Calculator /*extends Object*/ implements Serializable{

    private static final long serialVersionUID = 2844180938609224735L;

    //public static final long serialVersionUID =  87534658654654L;

    static {
        System.out.println("static block");
    }

    protected Memory memory = new ListMemory();

    {
        try {

           /* File dir = new File("target" + File.separator + ".." + File.separator + "target" + File.separator + "calc");
            if (!dir.exists()) {
                System.out.println("creating " + dir.getCanonicalPath());
                dir.mkdirs();
            } else {
                System.out.println("folder " + dir.getAbsolutePath() + " already exists");
            }

            File file = new File(dir, "calc.memory" );
            if(!file.exists()){
                System.out.println("creating file " + file.getCanonicalPath());
                file.createNewFile();
            } else {
                System.out.println("file " + file.getCanonicalPath() + " already exists");
            }

            for( String fileName : dir.list( (d, n)->n.endsWith(".memory") )){
                System.out.println("file name: " + fileName);
            }*/

            Path dirPath = Paths.get("target", "calc");

            if(!Files.exists(dirPath)){
                Files.createDirectory(dirPath);
            }

            Path filePath = Paths.get(dirPath.toString(), "calc.memory");
            if(!Files.exists(filePath)){
                Files.createFile(filePath);
            }


        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public Calculator(double result){
        // super for Object
        super();
        memory.setValue(result);
    }

    /*public Calculator(){
        this(0);
        //this.result = 0;
    }*/

    public abstract void displayResult();

    public void saveOld(){
        System.out.println("saving value " + memory.getValue());

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("target/calc/calc.memory", false));
            bw.write(Double.toString(memory.getValue()));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(bw!=null){
                    bw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveToFile(){
        System.out.println("saving value " + memory.getValue());

        // try-with-resources
        try(BufferedWriter bw = new BufferedWriter(new FileWriter("target/calc/calc.memory", false))){
            bw.write(Double.toString(memory.getValue()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveSerialized(){

        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("target/calc/calc.serialized"))){
            oos.writeObject( this );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save(){

        try(Connection c = getConnection();){

            c.setAutoCommit(false);

            PreparedStatement stmt = c.prepareStatement("insert into memory (value) values (?)");

            for(double value : memory.getValues()) {
                stmt.setDouble(1, value);
                //stmt.addBatch();
                stmt.executeUpdate();
            }

            c.commit();
            //stmt.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    private Connection getConnection(){

        try {
            Class.forName("org.h2.Driver");
            return DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "sa", "");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void loadFromDB(){

        try(Connection c = getConnection()){
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("select value from memory order by id desc limit 1");

            while(rs.next()){
                double d = rs.getDouble("value");
                System.out.println("value from db: " + d);
                memory.setValue(d);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Calculator load(){

        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("target/calc/calc.serialized"))){
            Object o = ois.readObject();
            Calculator c = (Calculator) o;
            return c;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new BetterCalculator();
    }


    public void loadFromFile(){

        try(BufferedReader br = new BufferedReader(new FileReader("target/calc/calc.memory"))){
            String stringValue = br.readLine();
            memory.setValue(Double.parseDouble(stringValue));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public double getResult() { // JavaBeans , POJO = Plain Old Java Object
        return memory.getValue();
    }


   /* public void setResult(double result) {
        System.out.println("changing value from " + this.result + " to  " + result);
        this.result = result;
    }*/


    public void add(double o1, double... operand){ // var-args
        memory.setValue(memory.getValue() + o1);
        for(double o : operand) {
            memory.setValue(memory.getValue() + o);
        }
    }

    public void subtract(double operand){
        memory.setValue( memory.getValue() - operand );
    }

    public void multiply(double operand ){
        memory.setValue( memory.getValue() * operand );
    }

    public void divide(double operand) throws CalculatorException{
        assert(operand!=0) : "operand is zero";
        if(operand==0){
            throw new CalculatorException("pamietaj cholero nie dziel przez zero");
        }
        memory.setValue( memory.getValue() / operand );
    }


    public static int multiply(int x, int y){
        return x * y;
    }


    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
