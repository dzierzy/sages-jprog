package pl.com.sages.jprog.calculator;

public class CalculatorException extends RuntimeException {

    public CalculatorException(String message) {
        super(message);
    }
}
