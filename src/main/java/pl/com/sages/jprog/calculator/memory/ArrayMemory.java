package pl.com.sages.jprog.calculator.memory;

import java.util.Arrays;

public class ArrayMemory implements Memory{

    private Double[] values = new Double[5];

    private int index = 0;

    @Override
    public double getValue() {
        return values[index];
    }

    @Override
    public void setValue(double value) {

        //    [ 0,1,2,3,4 ] + 5
        // 1. [ 5,1,2,3,4 ] i=0
        // 2. [ 0,1,2,3,4,5 ] i=5
        // 3. [ 1,2,3,4,5 ] i=4

        index = (index+1)%5; // strategy no 1
        values[index] = value;
    }

    @Override
    public String toString() {

        String arrayString = "";
        StringBuilder sb = new StringBuilder();
        for(Double d : values){
            //arrayString += d;
            sb.append(d).append(",");
        }

        return "ArrayMemory{" +
                "values=" + sb.toString() +
                ", index=" + index +
                '}';
    }
}
