package pl.com.sages.jprog.calculator.better;

import pl.com.sages.jprog.calculator.Calculator;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class BetterCalculator extends Calculator {

    private static final long serialVersionUID = 4444847744049442718L;

    {
        System.out.println("better calculator: initialization block");
    }

    public BetterCalculator(){
        super(0);
    }

    public BetterCalculator(double result){
        super(result);
        System.out.println("bettercalculator parametrized constructor");
    }

    @Override
    public void subtract(double operand){
        memory.setValue( memory.getValue() - operand );
    }

    @Override
    public void displayResult() {
        //java.util
        Calendar calendar = Calendar.getInstance();
        calendar.set(2019, 3, 22, 16, 12, 00);
        calendar.add(Calendar.MONTH,10);
        Date date = calendar.getTime();
        Locale locale = new Locale("en", "PL");
        DateFormat df = new SimpleDateFormat("yyyy?MM?dd G w D E", locale);

        // java.time
        LocalDateTime ldt = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy?MM?dd G w D E", locale);
        String dateString = dtf.format(ldt);

        // number format
        NumberFormat nf = NumberFormat.getInstance(locale);
        nf.setMaximumFractionDigits(5);
        nf.setMinimumFractionDigits(2);
        String valueString = nf.format(memory.getValue());

        Locale.setDefault(new Locale("zh"));

        ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("en", "PL"));

        System.out.println(
                "[" + dateString + "] " + bundle.getString("result.label") + ":" + valueString
        );

        long startNano = System.nanoTime();
        Duration d = Duration.between(LocalDateTime.now(), LocalDateTime.of(2020,12,12,12,12,12,12));
        System.out.println("duration: " + d.getSeconds() + "," + d.getNano());
        long endName = System.nanoTime();

       /* try {
            date = df.parse("2020?02?22 dopo Cristo 8 53 sab");
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
    }

    public static int multiply(int x, int y){
        return x * y;
    }

    public void power(int n){
        memory.setValue(
                Math.pow(
                        memory.getValue(),
                        n
                )
        );
    }

}


