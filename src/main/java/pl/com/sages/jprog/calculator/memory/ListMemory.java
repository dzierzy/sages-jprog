package pl.com.sages.jprog.calculator.memory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory, Serializable {

    private static final long serialVersionUID = 6687201961956496274L;

    private List<Double> values = new ArrayList<>();

    @Override
    public double getValue() {
        return values.get(values.size()-1);
    }

    @Override
    public void setValue(double value) {
        values.add(value);
    }


    @Override
    public List<Double> getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "ListMemory{" +
                "values=" + values +
                '}';
    }
}
