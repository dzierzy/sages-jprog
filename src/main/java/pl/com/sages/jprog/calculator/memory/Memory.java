package pl.com.sages.jprog.calculator.memory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public interface Memory {

    double getValue();

    default List<Double> getValues(){
        return new ArrayList<>();
    }

    void setValue(double value);
}
