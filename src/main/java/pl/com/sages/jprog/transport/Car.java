package pl.com.sages.jprog.transport;

public class Car implements Transportation {

    @Override
    public void transport(String passengerName) {
        System.out.println("car is transporting passenger " + passengerName);
    }

    @Override
    public int getRange() {
        return 1000;
    }
}
