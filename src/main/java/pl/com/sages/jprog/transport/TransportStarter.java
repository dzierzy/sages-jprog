package pl.com.sages.jprog.transport;

import pl.com.sages.jprog.zoo.Horse;

public class TransportStarter {

    public static void main(String[] args) {
        System.out.println("TransportStarter.main");

        String passenger = "Jan Kowalski";

        Transportation t =  p -> System.out.println("teleporting passenger " + p);
                //new Horse();
        t.transport(passenger);


    }
}
