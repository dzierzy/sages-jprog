package pl.com.sages.jprog.transport;

@FunctionalInterface
public interface Transportation {

    void transport(String passengerName);

    default int getSpeed(){
        return 60;
    }

    default int getRange(){
        return 500;
    }

}
