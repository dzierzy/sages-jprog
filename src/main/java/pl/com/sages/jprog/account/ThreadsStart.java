package pl.com.sages.jprog.account;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadsStart {

    public static void main(String[] args) {

        System.setOut(new ThreadNamePrefixPrintStream(System.out));

        System.out.println("ThreadsStart.main");

        ExecutorService es = Executors.newFixedThreadPool(1);

        Account account = new Account();

        es.execute(new DepositTask(account, 1000));
        es.execute(new WithdrawTask(account, 1000));

       /* Thread t1 = new Thread(new DepositTask(account, 1000));
        Thread t2 = new Thread(new WithdrawTask(account, 1000));
        t1.start();
        t2.start();*/

        System.out.println("done.");

        es.shutdown();


    }

}
