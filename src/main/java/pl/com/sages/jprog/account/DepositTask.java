package pl.com.sages.jprog.account;

public class DepositTask implements Runnable {

    private Account account;
    private int repeat;

    public DepositTask(Account account, int repeat) {
        this.account = account;
        this.repeat = repeat;
    }

    @Override
    public void run() {

        for(int i=0; i<repeat; i++){
            account.deposit(1);
        }

        System.out.println("[deposit] account balance: " + account.getBalance());

    }
}
