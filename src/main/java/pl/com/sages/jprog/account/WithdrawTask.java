package pl.com.sages.jprog.account;

public class WithdrawTask implements Runnable {

    private Account account;

    private int repeat;

    public WithdrawTask(Account account, int repeat) {
        this.account = account;
        this.repeat = repeat;
    }

    @Override
    public void run() {

        for(int i=0; i<repeat; i++){
            account.withdraw(1);
        }

        System.out.println("[withdraw] account balance: " + account.getBalance());
    }
}
