package pl.com.sages.jprog.account;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {

    private AtomicInteger balance = new AtomicInteger(1_000_000);

    public boolean withdraw(int amount){

        /*synchronized (this) {*/
            if (balance.get() >= amount) {
                balance.addAndGet(-amount);//balance = balance - amount; 1.read balance 2.calculate 3.assign value
                return true;
            } else {
                return false;
            }
        /*}*/
    }

    public void deposit(int amount){
        //balance+=amount;
        balance.addAndGet(amount);
    }



    public int getBalance() {
        return balance.get();
    }
}
