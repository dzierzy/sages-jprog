package pl.com.sages.jprog.javafx.nameday;

public class NameDayInfo {

    public boolean isYourNameDay(String yourName){
        // hardcoded
        return "Adam".equalsIgnoreCase(yourName) || "Ewa".equalsIgnoreCase(yourName);
    }
}
