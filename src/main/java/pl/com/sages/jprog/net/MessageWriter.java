package pl.com.sages.jprog.net;

import java.io.PrintWriter;
import java.util.Scanner;

public class MessageWriter implements Runnable {

    private PrintWriter writer;

    public MessageWriter(PrintWriter writer) {
        this.writer = writer;
    }

    @Override
    public void run() {
        System.out.println("writer started.");
        Scanner scanner = new Scanner(System.in);
        String msg = "";
        while(!"quit".equalsIgnoreCase(msg)){
            msg=scanner.nextLine();
            writer.println(msg);
            writer.flush();
        }
        System.out.println("[info] exiting...");
        writer.close();
    }
}
