package pl.com.sages.jprog.net;

import java.io.BufferedReader;
import java.io.IOException;

public class MessageReader implements Runnable {

    private BufferedReader reader;

    public MessageReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {
        System.out.println("reader started.");
        String received = null;
        try(BufferedReader myReader = reader) {
            while((received = myReader.readLine())!=null && !"quit".equals(received)){
                System.out.println("< " + received + " >");
            }
        } catch (IOException e) {
        }
    }
}
