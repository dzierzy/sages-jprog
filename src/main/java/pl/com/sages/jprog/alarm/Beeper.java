package pl.com.sages.jprog.alarm;



public class Beeper implements Runnable {

    public void beep(){

        for(int i = 0; i < 20; i++){
            System.out.println("beep");

            if(i>5){
                throw new RuntimeException("fake exception");
            }
            try {
                Thread.sleep(1*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        beep();
    }
}
