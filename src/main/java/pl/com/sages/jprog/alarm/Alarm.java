package pl.com.sages.jprog.alarm;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Alarm {

    public static void main(String[] args) {


        ScheduledExecutorService es = Executors.newScheduledThreadPool(5);

        Beeper b = new Beeper();
        Light l = new Light();

        es.schedule(b, 10, TimeUnit.SECONDS);
        es.execute(l);
       /* Thread t1 = new Thread(b);
        Thread t2 = new Thread(l);
        t1.start();
        t2.start(); */



        es.shutdown();



        System.out.println("done.");

    }
}
