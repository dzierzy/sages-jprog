package pl.com.sages.jprog.alarm;


public class Light implements Runnable {

    public void light(){

        for(int i = 0; i<15; i++){
            System.out.println("flash");

            try {
                Thread.sleep(2*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        light();
    }
}
