package pl.com.sages.jprog.zoo;

public abstract class Fish extends Animal {

    @Override
    public void move() {
        System.out.println("fish is swimming...");
    }
}
