package pl.com.sages.jprog.zoo;

import pl.com.sages.jprog.transport.Transportation;

public final class Horse extends Animal implements Transportation  {

    @Override
    public void move() {
        System.out.println("horse is running!");
    }

    @Override
    public int getSize() {
        return 800;
    }


    @Override
    public void transport(String passengerName) {
        System.out.println("seating " + passengerName + " on  a horse");
        move();
    }
}
