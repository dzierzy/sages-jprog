package pl.com.sages.jprog.zoo;

import java.util.Comparator;

public class EmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee e1, Employee e2) {
        //return e1.getId() - e2.getId();
        return e1.getLastName().compareTo(e2.getLastName());
    }
}
