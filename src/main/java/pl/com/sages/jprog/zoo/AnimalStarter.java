package pl.com.sages.jprog.zoo;

import pl.com.sages.jprog.transport.Car;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnimalStarter {


    public static void main(String[] args) {
        System.out.println("AnimalStarter.main");//soutm

        Animal a = getAnimal();
        System.out.println("taking care of " + a);
        a.feed("tourist");
        a.move();

        Object o;


        //Set<Employee> employees = new TreeSet<>( (e1,e2)->e1.getLastName().compareTo(e2.getLastName())  );
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Jan", "Kowalski"));
         employees.add(new Employee(4, "Zbigniew", "Iksinski"));
        employees.add(new Employee(1, "Jan", "Kowalski"));
        employees.add(new Employee(2, "Adam", "Nowak"));
        employees.add(new Employee(3, "Krzysztof", "Kowalski"));

        Comparator<Employee> comparator = //new EmployeeComparator();
                /*new Comparator<Employee>() {
                    @Override
                    public int compare(Employee o1, Employee o2) {
                        return o1.getLastName().length()-o2.getLastName().length();
                    }
                };*/
                (e1, e2) -> e1.getLastName().length()-e2.getLastName().length();

        Stream<Employee> stream = employees.parallelStream();

        Consumer<String> consumer = System.out::println;
                //n-> System.out.println(n);

        //employees =
        //List<String> names =
        //boolean match =
        Optional<String> optionalName =
            stream.
                sorted((e1,e2)->e1.getId()-e2.getId()).
                filter( e -> !e.getLastName().equals("Kowalski")).
                map( e-> e.getLastName() ).
                //collect(Collectors.toList());
                peek(consumer).
                //anyMatch(n->n.length()>5);
                distinct().
                findFirst();


        String name = optionalName.orElseThrow( () -> new IllegalArgumentException("last name is missing"));
        System.out.println("name from optional: " + name);




        System.out.println("employess count = " + employees.size());
        System.out.println(employees);

       /* List<Employee> tmp = new ArrayList<>();
        for(Employee e : employees){
            System.out.println(e);
            if(!e.getLastName().equals("Kowalski")){
               tmp.add(e);
            }
        }
        employees = tmp;*/

        Iterator<Employee> itr = employees.iterator();
        while(itr.hasNext()){
            Employee e = itr.next();
            if(e.getLastName().equals("Kowalski")){
                itr.remove();
            }
        }

        employees.remove(new Employee(4,"Zbigniew", "Iksinski"));

        System.out.println(employees);
/*
        Employee e1 = null;
        Employee e2 = null;
        Employee e3 = null;

        int e1HashCode = e1.hashCode();
        int e2HashCode = e2.hashCode();

        boolean same = false;
        if(e1HashCode==e2HashCode){
            same = e2.equals(e1);
        }*/


    }

    public static Animal getAnimal(){
        return new Shark();
    }

    public static String getString(){
        return new String();
    }


    public static void takeAnimal(Object a){
        System.out.println(a);
    }

}