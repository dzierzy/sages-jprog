package pl.com.sages.jprog.zoo;

public class Eagle extends Bird {
    @Override
    public int getSize() {
        return 6;
    }
}
