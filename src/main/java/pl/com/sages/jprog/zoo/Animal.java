package pl.com.sages.jprog.zoo;

import pl.com.sages.jprog.transport.Transportation;

public abstract class Animal {


    boolean predator;



    public void feed(String food){
        System.out.println("eating " + food);
    }

    public abstract void move();

    public abstract int getSize();

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " of size " + getSize();
    }

    public boolean isPredator() {
        return predator;
    }

    public void setPredator(boolean predator) {
        this.predator = predator;
    }
}
