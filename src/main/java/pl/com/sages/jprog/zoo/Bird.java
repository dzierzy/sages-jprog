package pl.com.sages.jprog.zoo;

public abstract class Bird extends Animal {

    @Override
    public final void move() {
        System.out.println("bird is flying....");
    }
}
