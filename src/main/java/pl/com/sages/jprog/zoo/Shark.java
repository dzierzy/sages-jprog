package pl.com.sages.jprog.zoo;

public class Shark extends Fish {
    @Override
    public int getSize() {
        return 500;
    }
}
